package com.example.sqliteapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity:LOG";

    // Attention, si vous réexécutez plusieurs fois le programme il faut d'abord
    // supprimer la BDD ou changer la version (puisque le traitement supprime et recréé la BDD)
    public void sauveDonnées(SQLClient bdd){
        // Ouverture d'une connexion en écriture
        SQLiteDatabase dbW = bdd.getWritableDatabase();

        // Pour pouvoir stocker les données envoyées à la BDD (sans utilisation de SQL - cf plus bas)
        // Info d'une première personne
        ContentValues valeursClient1 = new ContentValues();
        valeursClient1.put("id", "1");
        valeursClient1.put("nom", "Chevalier");
        // Insertion dans la BDD
        dbW.insert("Clients", null, valeursClient1);

        //*************************************************************************
        // Info d'une deuxième personne
        ContentValues valeursClient2 = new ContentValues();
        valeursClient2.put("id", "2");
        valeursClient2.put("nom", "Julien");
        // Insertion dans la BDD
        dbW.insert("Clients", null, valeursClient2);

        //--------------------------------------------- Utilisation de SQL
        dbW.execSQL("insert into Clients values(25, 'Roberts');");

        // ferme la connexion en écriture à la BDD -- à vous de voir s'il faut ou non conserver la connexion ouverte ... Attention aux ressources...
        dbW.close();
    }

    public void litDonnées(SQLClient bdd){
        // Ouverture d'une connexion en lecture
        SQLiteDatabase dbR = bdd.getReadableDatabase();

        // Sans SQL (cf plus bas pour SQL)
        String [] critèresDeProjection = {"id", "nom"};
        String [] critèreDeSélection = {}; // Aucun critère donc tous les enregistrements

        // Ouvre un curseur avec le(s) résultat(s)
        Cursor curs = dbR.query("Clients", critèresDeProjection, "", critèreDeSélection, null, null, "nom DESC");

        // Traite les réponses contenues dans le curseur

        // Y'a t'il au moins un résultat ?
        if (curs.moveToFirst()) {
            // Parcours des résultats
            do {
                // Récupération des données par le numéro de colonne
                //long clientID = curs.getLong(0);
                // ou avec le nom de la colonne (sans doute à privilégier pour la relecture du code)
                long clientID = curs.getLong(curs.getColumnIndexOrThrow("id"));
                // déclenche une exception si la colonne n'existe pas cf doc pour autres méthodes disponibles
                String clientNOM = curs.getString(curs.getColumnIndexOrThrow("nom"));

                Log.v(MainActivity.TAG, clientID + " - " + clientNOM);

            } while (curs.moveToNext());
        }
        else{
            Toast.makeText(this, "Pas de réponses.....", Toast.LENGTH_SHORT).show();
        }

        //------------------------------------------------------------ Avec SQL
        Cursor cursSQL = dbR.rawQuery("select id, nom from Clients order by nom ASC", null);

        // Le traitement des résultats est similaire à haut dessus.
        // Y'a t'il au moins un résultat ?
        if (cursSQL.moveToFirst()) {
            // Parcours des résultats
            do {
                // Récupération des données par le numéro de colonne
                //long clientID = curs.getLong(0);
                // ou avec le nom de la colonne (sans doute à privilégier pour la relecture du code)
                long clientID = cursSQL.getLong(cursSQL.getColumnIndexOrThrow("id"));
                // déclenche une exception si la colonne n'existe pas cf doc pour autres méthodes disponibles
                String clientNOM = cursSQL.getString(cursSQL.getColumnIndexOrThrow("nom"));

                Log.v(MainActivity.TAG, clientID + " - " + clientNOM);

            } while (cursSQL.moveToNext());
        }
        else{
            Toast.makeText(this, "Pas de réponses.....", Toast.LENGTH_SHORT).show();
        }

        // ferme la connexion en lecture à la BDD -- à vous de voir s'il faut ou non conserver la connexion ouverte ... Attention aux ressources...
        dbR.close();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // une instance de SQLClient suffit pour une activité et pour l'ensemble des tables...
        SQLClient bdd = new SQLClient(this);

        // Illustration de l'écriture de données dans la BDD
        this.sauveDonnées(bdd);

        //########################################################################
        // Illustration de la lecture de données dans la BDD
        this.litDonnées(bdd);

        // FINISH : Ferme l'instance de BDD ainsi que toutes les connexions ouvertes
        bdd.close();
    }
}


// Il faudrait encapsuler les traitements liés à la BDD dans un thread spécifique
// pour ne pas bloquer le thread UI
class SQLClient extends SQLiteOpenHelper{

    // Vous devez gérer le numéro de version de votre BDD (a un impact sur la reconstruction de la BDD par exemple)
    public static final int DATABASE_VERSION = 5;

    // Nom du fichier contenant la BDD (sqlite = fichier)
    public static final String  DATABASE_FILE = "clients.db";

    // Requete de creation de la bdd (exemple simplifié)
    public static final String SQL_CREATE = "CREATE TABLE Clients (id INTEGER PRIMARY KEY, nom TEXT);";

    // Requete de suppression de la bdd (exemple simplifié)
    public static final String SQL_DELETE = "DROP TABLE IF EXISTS  Clients ;";


    // Constructeur permettant d'appeler le constructeur de SQLIteOpenHelper (cf. doc)
    public SQLClient(Context context){
        super (context, DATABASE_FILE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // On créé la BDD si besoin
        db.execSQL(SQLClient.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Si la version de la BDD change.. Ici doit être mis le code pour traiter cette situation
        // Ici : traitement violent... On supprimme et on la créé à nouveau...
        // A adapter en fonction des besoins....
        db.execSQL(SQLClient.SQL_DELETE);
        this.onCreate(db);
    }
}